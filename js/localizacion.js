var local=(function (){
    var url=" https://vision.googleapis.com/v1/images:annotate?key=AIzaSyATTIUYuDBH47upxh-tEXDWWzsbMBHxx_o"
    var _get=function(){
    	var parametros=`{  "requests": [
    {
      "image":{
        "source":{
          "imageUri":
            "https://raw.githubusercontent.com/ZunigaFloresJoseluis/layar001/master/img/IMG_20180223_151526.jpg"
        }
      },
      "features": [
        {
          "type": "LABEL_DETECTION"
        }
      ]
    }
  ]
}`
    	
    	var  xhr=new XMLHttpRequest()
    	xhr.onreadystatechange=cambio(xhr)
    	xhr.open('POST', url);
         xhr.setRequestHeader("Content-Type", "application/json");
     //    xhr.setRequestHeader("Access-Control-Allow-Origin","*")
       //  xhr.setRequestHeader("Content-Length", parametros.length);
      //   xhr.setRequestHeader("Connection", "close");
         xhr.setRequestHeader("Accept","application/json")
         xhr.send(parametros);

    }

    var cambio=function(xhr){
    	return function(){
    		console.log('--->')
    		if(xhr.readyState==4){
    			if (xhr.state==200) {
    				console.log(':)')
    			}
    			else{
    				console.log(JSON.parse(xhr.response))
    				_rellenar_tabla(JSON.parse(xhr.response))
    			}
    		}
    	}
    }

    var _rellenar_tabla=function (obj_respuesta) {
    	var tabla=document.getElementById("etiqueta").children[1]
    	var length=obj_respuesta.responses[0].labelAnnotations.length
    	for(let i=0;i<length;i++){
    		var tr=document.createElement('tr')
    		var num=document.createElement('td')
    		num.textContent=i;
    		var td=document.createElement('td')
    		td.textContent=obj_respuesta.responses[0].labelAnnotations[i].description
    		var td2=document.createElement('td')
    		td2.textContent=obj_respuesta.responses[0].labelAnnotations[i].score
    		
    		
    		tr.appendChild(num)
    		tr.appendChild(td)
    		tr.appendChild(td2)
    		tabla.appendChild(tr)
    	}
    }

	return {
		get:_get
	}
}())

